﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text;

namespace WindowsFormsApplication1
{
    class ModelBox
    {
        Monitor monitor;
        Timer DrawTimer;
        Timer GlobalTimer;
        Robot robot;
        World world;
        public ModelBox(Monitor M)
        {
            monitor = M;
            DrawHelper.monitor = M;

            world = new World();
            robot = new Robot();

            GlobalTimer = new Timer();
            GlobalTimer.Interval = 1000;
            GlobalTimer.Tick += new EventHandler(TimerTick);
            

            DrawTimer = new Timer();
            DrawTimer.Interval = 20;
            DrawTimer.Tick += new EventHandler(TimerUpdate);
            DrawTimer.Start();
            GlobalTimer.Start();

        }

        void TimerTick(object sender, EventArgs e)
        {
            WorldHelper.Seconds += 1;
            Fps = DrawCount;
            DrawCount = 0;
        }


        int Fps;
        void DrawFps()
        {
            monitor.DrawText("fps:" + Fps.ToString());

        }


        void TimerUpdate(object sender, EventArgs e)
        {

            
            DrawTimer.Stop();
            monitor.Clear();

            Update();
            
            Draw();
            DrawFps();

            

            monitor.Refresh();
            DrawTimer.Start();
        }

        void Update()
        {
            //Update стандартно 2 раза за вызов
            for (int i = 0; i < 2; i++)
            {
                robot.Update();
            }
        }

        int DrawCount;
        void Draw()
        {
            world.Draw();
            robot.Draw();
            DrawCount += 1;
        }


    }
}
