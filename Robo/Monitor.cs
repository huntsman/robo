﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Monitor : UserControl
    {
        Bitmap bm;
        Graphics graf;
        public Point MSize
        {
            get
            {
                return (Point)pictureBox1.Size;
            }
        }
        public Monitor()
        {
            InitializeComponent();



            bm = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            graf = Graphics.FromImage(bm);
        }


        public void Draw(Image img, Point point)
        {
            graf.DrawImage(img, point);
            pictureBox1.Image = bm;
        }

        public void DrawText(string Text)
        {
            Font f = new Font("Arial",10f);
            //f.Size = 10f;
            //f.Name = "Arial";
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            graf.DrawString(Text, f, drawBrush, new PointF());
        }

        public void Clear()
        {

            graf.Clear(Color.White);
            
        }


        public void Refresh()
        {
            pictureBox1.Refresh();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Point p = (Point)sender;

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {

            textBox2.Text = e.X.ToString() + ";" + e.Y.ToString();
            textBox1.Text = WorldHelper.WorldBitmap.GetPixel(e.X, e.Y).ToString();
            
        }
        
    }
}
