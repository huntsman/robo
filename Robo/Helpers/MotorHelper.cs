﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    public static class MotorHelper
    {
        public static float _angle;

        public static float Angle
        {
            get
            {
                return _angle;
            }
            set
            {
                _angle = value;

                if (value < 0)
                    _angle = 360 + value;
                    
                if (value >= 360)
                    _angle = 0;
            }
        }
        public static float MaxVolts;
        public static float MaxSpeed;
        public static int Friction; //трение в процентах
        public static List<MotorsGroup> Groups;
    }
}
