﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace WindowsFormsApplication1
{
    public static class WorldHelper
    {
        public static int Seconds;
        public static Bitmap WorldBitmap;
        public static List<Object> Objects;


        public static void Draw()
        {
            foreach (Object wo in WorldHelper.Objects)
            {
                wo.Draw();
            }
        }

        public static int ObjectIndex(ObjName name)
        {
            for (int i = 0; i < WorldHelper.Objects.Count; i++)
            {
                if (WorldHelper.Objects[i].Name == name)
                    return i;
            }

            return (-1);
        }

        public static PointF GetCenter(int ObjIndex)
        {
            PointF TempPoint = new PointF();
            int tp = WorldHelper.Objects[ObjIndex].Rectangle.Height / 2;
            TempPoint.Y = WorldHelper.Objects[ObjIndex].ForRotation.Y + tp;
            tp = WorldHelper.Objects[ObjIndex].Rectangle.Width / 2;
            TempPoint.X = WorldHelper.Objects[ObjIndex].ForRotation.X + tp;
            return TempPoint;
        }

        public static PointF GetRotationCenter(int ObjIndex)
        {
            return WorldHelper.Objects[ObjIndex].GetRotateCenter();
        }

    }
}
