﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace WindowsFormsApplication1
{
    class Robot
    {
        Bitmap Bitm;
        //Bitmap Bitmt;
        Graphics graf;
        public int ObjIndex;

        //Тут добавляем элементы
        LineSensor LeftSensor;
        LineSensor RightSensor;

        public Motor LeftMotor;
        public Motor RightMotor;

        Logics logics;


        #region Signals
        public double LeftSensorData;
        public double RightSensorData;

        #endregion


        public Robot()
        {
            ObjIndex = WorldHelper.ObjectIndex(ObjName.Robot);

            MotorHelper.Friction = 100;
            MotorHelper.MaxVolts = 20f;
            MotorHelper.MaxSpeed = 1f;

            logics = new Logics(this);
            Bitm = new Bitmap(WorldHelper.Objects[ObjIndex].Image.Width, WorldHelper.Objects[ObjIndex].Image.Height);
            //Bitmt = new Bitmap(img.Width, img.Height);
            graf = Graphics.FromImage(Bitm);

            //Определяем элементы
            LeftSensor = new LineSensor(ObjName.SensorLeft);
            RightSensor = new LineSensor(ObjName.SensorRight);

            LeftSensor.SignalEvent += new LineSensor.NewSignal(LeftSensor_SignalEvent);
            RightSensor.SignalEvent += new LineSensor.NewSignal(RightSensor_SignalEvent);

            MotorHelper.Groups = new List<MotorsGroup>();
            

            LeftMotor = new Motor(ObjName.MotorLeft);
            RightMotor = new Motor(ObjName.MotorRight);

            //LeftMotor.Volt = 5;
            //RightMotor.Volt = 0;

            MotorsGroup mg = new MotorsGroup(LeftMotor.ObjIndex, RightMotor.ObjIndex);
            MotorHelper.Groups.Add(mg);

            //Собираем робота
            RobotCollect();
        }

        void RightSensor_SignalEvent(double Volt)
        {
            RightSensorData = Volt;
        }

        void LeftSensor_SignalEvent(double Volt)
        {
            LeftSensorData = Volt;
        }

        void RobotCollect()
        {
            graf.DrawImage(WorldHelper.Objects[ObjIndex].Image, new Point(0, 0));

            //graf.DrawImage(WorldHelper.Objects[LeftSensor.ObjIndex].Image, new Point(24, 5));
            //graf.DrawImage(WorldHelper.Objects[RightSensor.ObjIndex].Image, new Point(64, 5));

            graf.DrawImage(WorldHelper.Objects[LeftMotor.ObjIndex].Image, new Point(2, 29));
            graf.DrawImage(WorldHelper.Objects[RightMotor.ObjIndex].Image, new Point(68, 29));
        }

        public void Draw()
        {

            DrawHelper.Draw(rotateImage(Bitm,MotorHelper.Angle), WorldHelper.Objects[ObjIndex].Rectangle);

            foreach (Object obj in WorldHelper.Objects)
            {
                if (obj.Name == ObjName.SensorLeft | obj.Name == ObjName.SensorRight)
                    DrawHelper.Draw(obj.Image, obj.Rectangle);

                //if (obj.Name == ObjName.SensorRight)
                //    DrawHelper.Draw(obj.Image, obj.Rectangle);

            }

        }

        private Bitmap rotateImage(Bitmap b, float angle)
        {
            //create a new empty bitmap to hold rotated image
            Bitmap returnBitmap = new Bitmap(b.Width, b.Height);
            //make a graphics object from the empty bitmap
            Graphics g = Graphics.FromImage(returnBitmap);
            //move rotation point to center of image
            g.TranslateTransform((float)b.Width / 2, (float)b.Height / 2);
            //rotate
            g.RotateTransform(angle);
            //move image back
            g.TranslateTransform(-(float)b.Width / 2, -(float)b.Height / 2);
            //draw passed in image onto graphics object
            g.DrawImage(b, new Point(0, 0));
            return returnBitmap;
        }


        public void Update()
        {
            LeftSensor.Update();
            RightSensor.Update();


            LeftMotor.Update();
            RightMotor.Update();

            foreach (MotorsGroup mg in MotorHelper.Groups)
            {
                mg.Update();
            }

            logics.Update();
        }
    }
}
