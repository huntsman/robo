﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    class Logics
    {
        Robot robo;
        public Logics(Robot r)
        {
            robo = r;
        }


        void GoLeft()
        {
            robo.LeftMotor.Volt = -7;
            robo.RightMotor.Volt = 7;
        }

        void GoRight()
        {
            robo.LeftMotor.Volt = 7;
            robo.RightMotor.Volt = -7;
        }

        void GoTop()
        {
            robo.LeftMotor.Volt = 7;
            robo.RightMotor.Volt = 7;
        }


        enum d
        {
            Left,
            Right,
            Top
        }

        Random rnd = new Random();
        public void Update()
        {
            
            bool ff = false;

            if (robo.LeftSensorData <= 0.3)
            {
                GoLeft();
                ff = true;
            }
            if (robo.RightSensorData <= 0.3)
            {
                GoRight();
                ff = true;
            }



            if (!ff)
            {
                robo.LeftMotor.Volt = 7;
                robo.RightMotor.Volt = 7;
                ff = true;
            }

        }
    }
}
