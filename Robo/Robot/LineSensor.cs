﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace WindowsFormsApplication1
{
    class LineSensor
    {
        public delegate void NewSignal(double Volt);
        public event NewSignal SignalEvent;
        //public ObjName Name;
        public int ObjIndex;
        Point Center;
        //public double SensorMaxValue;
        public double SensorMinValue;


        //Signal signal;
        public LineSensor(ObjName name)
        {
            //SensorMaxValue = 1;
            SensorMinValue = 0.4;
            //signal = new Signal();


            ObjIndex = WorldHelper.ObjectIndex(name);
            NewCenter();

        }

        void NewCenter()
        {
            Center = GetCenter();

        }

        public void RandomSignal()
        {
            Random rnd = new Random();
            SignalEvent(rnd.Next(4, 10) * 0.1);
        }

        public void NullSignal()
        {
            Random rnd = new Random();
            SignalEvent(0 + (rnd.Next(0,3) * 0.1));
        }

        Point GetCenter()
        {
            Point TempPoint = new Point();
            int tp = WorldHelper.Objects[ObjIndex].Rectangle.Height / 2;
            TempPoint.Y = WorldHelper.Objects[ObjIndex].Rectangle.Y + tp;
            tp = WorldHelper.Objects[ObjIndex].Rectangle.Width / 2;
            TempPoint.X = WorldHelper.Objects[ObjIndex].Rectangle.X + tp;
            return TempPoint;
        }


        RGB GetPixel(Point point)
        {


            // Блокируем изоображение
            System.Drawing.Imaging.BitmapData bmpData = WorldHelper.WorldBitmap.LockBits(new Rectangle(0, 0, WorldHelper.WorldBitmap.Width, WorldHelper.WorldBitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, WorldHelper.WorldBitmap.PixelFormat);

            byte[] msv = new byte[bmpData.Stride * bmpData.Height];

            System.Runtime.InteropServices.Marshal.Copy(bmpData.Scan0, msv, 0, msv.Length);

            //int img_square_size = bmpData.Stride * bmpData.Height;

            //for (int y = 0; y < img_square_size; y++)
            //{
            //    for (int x = 0; x < img_square_size; x++)
            //    {
            //        //// Кодируем без карты перестановок
            //        //EncodingCardsOutPermutations(ref rgb_array, ref k);

            //        // Устанавливаем цвета пикселей в LockBits
            //         = (byte)rgb_array[0]; // R
            //         = (byte)rgb_array[1]; // G
            //        = (byte)rgb_array[2]; // B
            //    }
            //}

            //Color color = Color.FromArgb(msv[bmpData.Stride * point.Y + 3 * point.X + 2], msv[bmpData.Stride * point.Y + 3 * point.X + 1], msv[bmpData.Stride * point.Y + 3 * point.X + 0]);

            //// Сохраним значение битов в LockBits
            //System.Runtime.InteropServices.Marshal.Copy(msv, 0, bmpData.Scan0, bmpData.Stride * bmpData.Height);
            RGB rgm = new RGB();

            rgm.R = msv[bmpData.Stride * point.Y + 3 * point.X + 2];
            rgm.G = msv[bmpData.Stride * point.Y + 3 * point.X + 1];
            rgm.B = msv[bmpData.Stride * point.Y + 3 * point.X + 0];

            // Разблокируем изоображение
            WorldHelper.WorldBitmap.UnlockBits(bmpData);



            return rgm;
        }



        public void Update()
        {
            NewCenter();
            //Point t = new Point(259,148);

            //RGB c = GetPixel(Center);
            Color col = WorldHelper.WorldBitmap.GetPixel(Center.X, Center.Y);
            //if (col.Equals(Color.Black))
            if (col.A == 255 && col.R == 0 && col.G == 0 && col.B == 0)
                NullSignal();
            else
                RandomSignal();
                
        }


    }

    class RGB
    {
        public byte R;
        public byte G;
        public byte B;
    }
}
