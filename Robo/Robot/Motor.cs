﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace WindowsFormsApplication1
{
    class Motor
    {
        public int ObjIndex;
        public float Volt;


        public Motor(ObjName name)
        {

            ObjIndex = WorldHelper.ObjectIndex(name);
            Volt = 0;
        }


        public void Energize(float volt)
        {
            Volt = volt;
        }

        float CalcSpeed(float volt)
        {
            bool minus = false;
            if (volt < 0)
                minus = true;
            volt = Math.Abs(volt);
            int prt = (int)((volt / MotorHelper.MaxVolts) * 100); //volt от MotorHelper.MaxVolts в процентах
            if (prt > 100) //Заглушка
                prt = 100;
            float speed = (MotorHelper.MaxSpeed / 100) * prt; //Собственно находим скорость

            //volt *= 0.1; //Переводим в псевдо см/с
            ////volt = (volt / 100) * MotorHelper.Friction; //Применяем псевдо трение
            ////volt *= WorldHelper.GlobalTime; //Скорость на время, стандарт. формула
            if (minus)
                speed *= (-1);

            return speed;
        }

        //void Modif( double volt)
        //{
        //    int x = 0;
        //    int y = 0;

        //    volt *= 10; //Переводим в псевдо см/с
        //    volt = (volt / 100) * MotorHelper.Friction; //Применяем псевдо трение

        //}

        public void Update()
        {
            foreach (MotorsGroup mg in MotorHelper.Groups)
            {
                if (mg.Exist(ObjIndex))
                {
                    CMotor cm;
                    if (WorldHelper.Objects[ObjIndex].Name == ObjName.MotorLeft)
                        cm = CMotor.Left;
                    else
                        cm = CMotor.Right;

                    mg.SetMotorSpeed(cm,CalcSpeed(Volt));
                }
            }
        }

    }
}
