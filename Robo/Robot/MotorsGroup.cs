﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace WindowsFormsApplication1
{
    public class MotorsGroup
    {
        int LeftMotorIndex;
        int RightMotorIndex;
        float LeftMotorSpeed;
        float RightMotorSpeed;
        int RoboIndex;
        public MotorsGroup(int LeftMotorIndex, int RightMotorIndex)
        {
            this.LeftMotorIndex = LeftMotorIndex;
            this.RightMotorIndex = RightMotorIndex;
            RoboIndex = WorldHelper.ObjectIndex(ObjName.Robot);
        }


        float tempangle;
        //PointF RLoacat; 
        void RotateObject(ObjName Name)
        {
            if (tempangle != MotorHelper.Angle)
            {


                int rl = WorldHelper.ObjectIndex(Name);
                PointF RoboCent = WorldHelper.GetCenter(RoboIndex);
                PointF PoboLoc = WorldHelper.GetCenter(rl);


                float tt = (float)Math.PI * (MotorHelper.Angle);


                float ang = tt / (float)180;

                PointF tl = new PointF();
                tl.X = (float)Math.Round((PoboLoc.X - RoboCent.X) * Math.Cos(ang) - (PoboLoc.Y - RoboCent.Y) * Math.Sin(ang)) + RoboCent.X;
                tl.Y = (float)Math.Round((PoboLoc.X - RoboCent.X) * Math.Sin(ang) + (PoboLoc.Y - RoboCent.Y) * Math.Cos(ang)) + RoboCent.Y;

                tl.X -= WorldHelper.Objects[rl].Rectangle.Width / 2;
                tl.Y -= WorldHelper.Objects[rl].Rectangle.Height / 2;

                //tl.X += WorldHelper.Objects[rl].ForRotation.X;
                //tl.Y += WorldHelper.Objects[rl].ForRotation.Y;

                WorldHelper.Objects[rl].Location = tl;
                //RLoacat = tl;

                //WorldHelper.Objects[rl].ClearRotation();
                //WorldHelper.Objects[rl].UpdateRotation(new PointF(tl.X - WorldHelper.Objects[rl].Location.X, tl.Y - WorldHelper.Objects[rl].Location.Y));
                
            }
        }

        public bool Exist(int MotorID)
        {
            if (LeftMotorIndex == MotorID)
                return true;
            else
            if(RightMotorIndex == MotorID)
                return true;

            return false;
        }


        void Move(ObjName Name, float speed)
        {
            if (speed != 0)
            {

                int rl = WorldHelper.ObjectIndex(Name);
                PointF RoboCent = WorldHelper.GetCenter(RoboIndex);
                PointF PoboLoc = WorldHelper.GetCenter(rl);


                float tt = (float)Math.PI * (MotorHelper.Angle + 270);


                float ang = tt / (float)180;

                PointF tl = new PointF();
                float Cos = speed * (float)Math.Cos(ang);
                float Sin = speed * (float)Math.Sin(ang);


                tl.X = (float)(WorldHelper.Objects[rl].Location.X +  Cos);
                tl.Y = (float)(WorldHelper.Objects[rl].Location.Y +  Sin);


                WorldHelper.Objects[rl].Location = tl;


                WorldHelper.Objects[rl].UpdateRotation(new PointF(Cos, Sin));

            }
        }

        public void SetMotorSpeed(CMotor Motor, float speed)
        {
            if (Motor == CMotor.Left)
                LeftMotorSpeed = speed;
            else
                RightMotorSpeed = speed;

        }

        void RotateAll()
        {
            RotateObject(ObjName.SensorLeft);
            RotateObject(ObjName.SensorRight);
            tempangle = MotorHelper.Angle; 
        }
        void MoveAll(float Speed)
        {
            Move(ObjName.Robot, Speed);
            Move(ObjName.SensorLeft, Speed);
            Move(ObjName.SensorRight, Speed);
        }

        public void Update()
        {
            float Speed = 0;
            

            //if (LeftMotorSpeed != 0 & RightMotorSpeed == 0)
            //{
            //    Speed = (LeftMotorSpeed + RightMotorSpeed) / 2;
            //    float ang = 2 * Speed;
            //    if (LeftMotorSpeed > 0)
            //        MotorHelper.Angle += ang;
            //    else
            //        MotorHelper.Angle -= ang;
            //    RotateAll();
            //}
            //else
            //if (LeftMotorSpeed == 0 & RightMotorSpeed != 0)
            //{
            //    Speed = (LeftMotorSpeed + RightMotorSpeed) / 2;
            //    float ang = 2 * Speed;
            //    if (RightMotorSpeed > 0)
            //        MotorHelper.Angle -= ang;
            //    else
            //        MotorHelper.Angle += ang;
            //    RotateAll();
            //}
            //else
            //if (LeftMotorSpeed != 0 & RightMotorSpeed != 0)
            //{
                

            //    if (LeftMotorSpeed > RightMotorSpeed)
            //    {
            //        float tspeed = (LeftMotorSpeed + RightMotorSpeed) / 2;
            //        LeftMotorSpeed = Math.Abs(LeftMotorSpeed);
            //        RightMotorSpeed = Math.Abs(RightMotorSpeed);
                    
            //        Speed = Math.Abs(tspeed);
            //        int prt = (int)(100 - ((RightMotorSpeed / (float)LeftMotorSpeed) * 100));
            //        float plus = ((0.1f + Speed) / 100f) * prt;

            //        if (tspeed > 0)
            //            MotorHelper.Angle += plus;
            //        else
            //            MotorHelper.Angle -= plus;


            //    }
            //    else
            //        if (LeftMotorSpeed < RightMotorSpeed)
            //        {
            //            MotorHelper.Angle = (100 - ((LeftMotorSpeed / (float)RightMotorSpeed) * 100)) / 7;

            //        }
            bool minus = false;
            bool mb = false;
            if (LeftMotorSpeed != 0 & RightMotorSpeed == 0)
            {
                Speed = (LeftMotorSpeed + RightMotorSpeed) / 2;
                float ang = 2 * Speed;
                if (LeftMotorSpeed > 0)
                    MotorHelper.Angle += ang;
                else
                    MotorHelper.Angle -= ang;

                mb = true;
            }
            else
                if (LeftMotorSpeed == 0 & RightMotorSpeed != 0)
                {
                    Speed = (LeftMotorSpeed + RightMotorSpeed) / 2;
                    float ang = 2 * Speed;
                    if (RightMotorSpeed > 0)
                        MotorHelper.Angle -= ang;
                    else
                        MotorHelper.Angle += ang;
                    mb = true;
                }
                else
            if (LeftMotorSpeed != 0 & RightMotorSpeed != 0)
            {


                
                float tspeed = (LeftMotorSpeed + RightMotorSpeed) / 2;

                if (LeftMotorSpeed < 0 | RightMotorSpeed < 0)
                    minus = true;

                float tlms = LeftMotorSpeed;
                LeftMotorSpeed = Math.Abs(LeftMotorSpeed);
                float trms = RightMotorSpeed;
                RightMotorSpeed = Math.Abs(RightMotorSpeed);

                Speed = Math.Abs((LeftMotorSpeed + RightMotorSpeed) / 2);

                int prt = 0;
                if (LeftMotorSpeed > RightMotorSpeed)
                    prt = (int)(100 - ((RightMotorSpeed / (float)LeftMotorSpeed) * 100));
                else
                    if (LeftMotorSpeed < RightMotorSpeed)
                        if (minus)
                            prt += (int)(100 - ((LeftMotorSpeed / (float)RightMotorSpeed) * 100));
                        else
                            prt -= (int)(100 - ((LeftMotorSpeed / (float)RightMotorSpeed) * 100));
                    else
                    {
                        mb = true;
                        if (tlms > trms)
                        prt += 100;
                        else
                            if (tlms < trms)
                                prt -= 100;
                            else
                            {
                                mb = false;
                            }

                        
                        
                    }

                float plus = ((0.1f + Speed) / 100f) * prt;


                    MotorHelper.Angle += plus;
                    


                
            }


            

            if (minus)
                Speed *= (-1);
            if (!mb)
                MoveAll(Speed);
                RotateAll();
                
            
        }
    }

    public enum CMotor
    {
        Left,
        Right
    }
}



////int motor1speed = ;
////int motor2speed = CalcSpeed(mot2volt);
//int Speed = 0;

////if (motor1speed == 0)
////{
////}
////if (motor2speed == 0)
////{
////}


//if (motor1speed > 0 | motor2speed > 0)  //Если хоть один мотор крутится
//{

//    if (motor1speed != 0 & motor2speed != 0) //Если оба мотора крутятся
//        if (motor1speed > motor2speed)
//        {
//            MotorHelper.Angle = (100 - ((motor2speed / (float)motor1speed) * 100)) / 7;

//            Speed = motor1speed - motor2speed;
//        }
//        else
//        if (motor1speed < motor2speed)
//        {
//            MotorHelper.Angle = (100 - ((motor1speed / (float)motor2speed) * 100)) / 7;
//            Speed = motor2speed - motor1speed;
//        }

//    if (motor1speed == 0) // Если Левый мотор не крутится
//    {
//        if (motor2speed > 0)
//            MotorHelper.Angle -= 10;
//        else
//            MotorHelper.Angle += 10;
//    }
//    if (motor2speed == 0) // Если правый мотор не крутится
//    {
//        if (motor1speed > 0)
//            MotorHelper.Angle += 10;
//        else
//            MotorHelper.Angle -= 10;
//    }



//    RotateObject(ObjName.SensorLeft);


//    if (motor1speed != 0 & motor2speed != 0) //Если оба мотора крутятся
//        if (motor1speed > motor2speed)
//        {
//            MotorHelper.Angle = (100 - ((motor2speed / (float)motor1speed) * 100)) / 7;
//        }
//        else
//            if (motor1speed < motor2speed)
//            {
//                MotorHelper.Angle = (100 - ((motor1speed / (float)motor2speed) * 100)) / 7;
//            }

//    Move(ObjName.Robot, Speed / 3);

//}



//tempangle = MotorHelper.Angle;
