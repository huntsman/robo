﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace WindowsFormsApplication1
{
    class World
    {
        Graphics graf;

        int FieldID;

        public World()
        {

            WorldHelper.Objects = new List<Object>();
            WorldHelper.WorldBitmap = new Bitmap(DrawHelper.monitor.MSize.X, DrawHelper.monitor.MSize.Y);

            graf = Graphics.FromImage(WorldHelper.WorldBitmap);

            LoadWorld();
            FieldID = WorldHelper.ObjectIndex(ObjName.Field);
        }

        void LoadWorld()
        {
            Point R = new Point(200, 200);

            NewObject(ObjName.Robot, Image.FromFile("./Images/Body.png"), new Rectangle(R.X, R.Y, 0, 0), false);
            NewObject(ObjName.SensorLeft, Image.FromFile("./Images/Sensor.png"), new Rectangle(R.X + 22, R.Y, 0, 0), true);
            NewObject(ObjName.SensorRight, Image.FromFile("./Images/Sensor.png"), new Rectangle(R.X + 49, R.Y, 0, 0), true);
            NewObject(ObjName.MotorLeft, Image.FromFile("./Images/Motor.png"), new Rectangle(0, 0, 0, 0), false);
            NewObject(ObjName.MotorRight, Image.FromFile("./Images/Motor.png"), new Rectangle(0, 0, 0, 0), false);

            NewObject(ObjName.Line, Image.FromFile("./Images/Line.png"), new Rectangle(100, 150, 0, 0), false);
            NewObject(ObjName.Field, Image.FromFile("./Images/Field.png"), new Rectangle(0, 0, DrawHelper.monitor.MSize.X, DrawHelper.monitor.MSize.Y), false);


        }

        public void NewObject(ObjName name, Image img, Rectangle rect)
        {
            Object wo = new Object(name, rect, img);
            WorldHelper.Objects.Add(wo);
        }
        public void NewObject(ObjName name, Image img, Rectangle rect, bool draw)
        {
            Object wo = new Object(name, rect, img);
            wo.ODraw = draw;
            WorldHelper.Objects.Add(wo);
        }



        public void Draw()
        {
            if (FieldID > (-1))
            {
                graf.DrawImage(WorldHelper.Objects[FieldID].Image, WorldHelper.Objects[FieldID].Location);
            }

            foreach (Object obj in WorldHelper.Objects)
            {
                if (obj.Name == ObjName.Line)
                    graf.DrawImage(obj.Image, obj.Location);
            }

            DrawHelper.Draw(WorldHelper.WorldBitmap, new Rectangle());

        }
        //public void Update()
        //{

        //}
    }
}
