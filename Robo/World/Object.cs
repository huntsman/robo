﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace WindowsFormsApplication1
{
    public class Object
    {
        public ObjName Name;
        Rectangle rect;
        Image img;
        PointF location;
        public PointF ForRotation;
        public PointF Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
                rect.X = (int)value.X;
                rect.Y = (int)value.Y;
            }
        }
        public Image Image
        {
            get
            {
                return img;
            }
        }
        public Rectangle Rectangle
        {
            get
            {
                return rect;
            }
        }
        public bool ODraw = true;
        public Object(ObjName Name, Rectangle rect, Image img)
        {
            this.Name = Name;
            this.rect = rect;
            this.img = img;
            Location = new PointF(rect.X, rect.Y);
            if (rect.Height == 0)
                this.rect.Height = img.Height;
            if (rect.Width == 0)
                this.rect.Width = img.Width;

            ForRotation = location;

        }

        public void UpdateRotation(PointF p)
        {
            ForRotation.X += p.X;
            ForRotation.Y += p.Y;
        }



        public void ClearRotation()
        {
            ForRotation.X = 0;
            ForRotation.Y = 0;
        }

        public PointF GetRotateCenter()
        {
            PointF TempPoint = new PointF();
            int tp = Rectangle.Height / 2;
            TempPoint.Y = (Location.Y + (float)tp) - ForRotation.Y;
            tp = Rectangle.Width / 2;
            TempPoint.X = (Location.X + (float)tp) - ForRotation.X;
            
            return TempPoint;
        }


        //public void intersect(Object obj)
        //{

        //    foreach (Object ob in WorldHelper.Objects)
        //    {

        //    }

        //}

        public void Draw()
        {
            if (ODraw)
            DrawHelper.Draw(img, rect);
        }
    }

    public enum ObjName
    {
        SensorLeft,
        SensorRight,
        Line,
        MotorLeft,
        MotorRight,
        Robot,
        Field,

    }
}
